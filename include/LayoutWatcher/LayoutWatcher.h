#pragma once

#include "LayoutWatcher_global.h"

#include <memory>
#include <string>
#include <string_view>
#include <vector>

#include <eventpp/callbacklist.h>

class IBackend;

class LAYOUTWATCHER_PUBLIC LayoutWatcher {
public:
	struct LayoutNames {
		std::string shortName;
		std::string displayName;
		std::string longName;

		bool operator==( const LayoutNames &rhs ) const {
			return shortName == rhs.shortName && displayName == rhs.displayName && longName == rhs.longName;
		}
	};

	[[maybe_unused]] LayoutWatcher();
	virtual ~LayoutWatcher();

	/**
	 * @brief Get current language layout
	 * @return short name of active layout
	 */
	virtual std::string_view getActiveLayout() const;
	/**
	 * @brief Get list of language layouts
	 * @return list of layouts
	 */
	virtual const std::vector<LayoutWatcher::LayoutNames> &getLayoutsList() const;

	// Signals
	eventpp::CallbackList<void( std::string_view )> onLayoutChanged;
	eventpp::CallbackList<void( const std::vector<LayoutWatcher::LayoutNames> & )> onLayoutListChanged;

protected:
	unsigned int layoutId_ = 0;
	std::vector<LayoutNames> layoutsList_;

private:
	std::unique_ptr<IBackend> backend_;

	void initializeBackendSignals();
};
