#pragma once

#if defined( _MSC_VER ) || defined( WIN64 ) || defined( _WIN64 ) || defined( __WIN64__ ) || defined( WIN32 ) || defined( _WIN32 ) || \
	defined( __WIN32__ ) || defined( __NT__ )
#	define LAYOUTWATCHER_EXPORT __declspec( dllexport )
#	define LAYOUTWATCHER_IMPORT __declspec( dllimport )
#else
#	define LAYOUTWATCHER_EXPORT __attribute__( ( visibility( "default" ) ) )
#	define LAYOUTWATCHER_IMPORT __attribute__( ( visibility( "default" ) ) )
#endif

#if defined( LAYOUTWATCHER_LIBRARY )
#	define LAYOUTWATCHER_PUBLIC LAYOUTWATCHER_EXPORT
#else
#	define LAYOUTWATCHER_PUBLIC LAYOUTWATCHER_IMPORT
#endif
