#include <chrono>
#include <iostream>
#include <string_view>
#include <thread>

#include "LayoutWatcher/LayoutWatcher.h"

static constexpr auto kTimeToTest = std::chrono::seconds( 10 );

std::ostream &operator<<( std::ostream &out, const std::vector<LayoutWatcher::LayoutNames> &list ) {
	for ( auto &&layout : list ) {
		out << " - " << layout.longName << "[" << layout.shortName << "] -- " << std::endl;
	}
	return out;
}

void onLayoutChanged( std::string_view name ) {
	std::cout << "Changed layout: " << name << std::endl;
}
void onLayoutListChanged( const std::vector<LayoutWatcher::LayoutNames> &list ) {
	std::cout << "Changed layout list: " << std::endl << list;
}

int main() {
	LayoutWatcher watcher;
	watcher.onLayoutChanged.append( onLayoutChanged );
	watcher.onLayoutListChanged.append( onLayoutListChanged );

	std::cout << "Current layout list:" << std::endl << watcher.getLayoutsList();
	std::cout << "Current layout: " << watcher.getActiveLayout() << std::endl;

	auto exit_time = std::chrono::steady_clock::now() + kTimeToTest;
	while ( std::chrono::steady_clock::now() < exit_time ) std::this_thread::sleep_for( std::chrono::seconds( 1 ) );

	std::cout << "Stop watching (timeout)" << std::endl;
	return 0;
}
