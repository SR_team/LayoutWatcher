#include "LayoutWatcher.h"

#include <chrono>
#include <iostream>
#include <memory>

#include <sdbus-c++/sdbus-c++.h>

#include "backends/Hyprland.h"

#ifdef BACKEND_KDE
#	include "backends/KDE.h"
#endif
#ifdef BACKEND_X11
#	include "backends/X11.h"
#endif
#ifdef BACKEND_GIO
#	include "backends/GIO.h"
#endif

using namespace std::chrono_literals;
namespace consts {
	constexpr auto kUpdateTime = 50ms;
} // namespace consts

static std::string xdg_session_desktop() {
	const char *desktop = std::getenv( "XDG_SESSION_DESKTOP" );
	return desktop != nullptr ? desktop : "";
}

LayoutWatcher::LayoutWatcher() {
	const auto session = xdg_session_desktop();
	if ( session == "Hyprland" ) {
		try {
			backend_ = std::make_unique<Hyprland>( consts::kUpdateTime );
			initializeBackendSignals();
			return;
		} catch ( std::exception &e ) {
			std::cerr << "Can't initialize KDE backend: " << e.what() << std::endl;
		}
	} else if ( session == "KDE" ) {
#ifdef BACKEND_KDE
		try {
			backend_ = std::make_unique<KDE>();
			initializeBackendSignals();
			return;
		} catch ( std::exception &e ) {
			std::cerr << "Can't initialize KDE backend: " << e.what() << std::endl;
		}
#endif
	} else if ( session == "GNOME" || session == "Unity" || session == "Budgie" || session == "MATE" || session == "XFCE" ||
				session == "Pantheon" || session == "Cinnamon" ) {
#ifdef BACKEND_GIO
		try {
			backend_ = std::make_unique<GIO>();
			initializeBackendSignals();
			return;
		} catch ( std::exception &e ) {
			std::cerr << "Can't initialize GIO backend: " << e.what() << std::endl;
		}
#endif
	}

#ifdef BACKEND_X11
	try {
		backend_ = std::make_unique<X11>( consts::kUpdateTime );
		initializeBackendSignals();
		return;
	} catch ( std::exception &e ) {
		std::cerr << "Can't initialize X11 backend: " << e.what() << std::endl;
	}
#endif
}

LayoutWatcher::~LayoutWatcher() {
	backend_.reset();
}

std::string_view LayoutWatcher::getActiveLayout() const {
	if ( layoutId_ < layoutsList_.size() ) return layoutsList_[layoutId_].shortName;
	return "";
}

const std::vector<LayoutWatcher::LayoutNames> &LayoutWatcher::getLayoutsList() const {
	return layoutsList_;
}

void LayoutWatcher::initializeBackendSignals() {
	if ( !backend_ ) return;

	backend_->onLayoutChanged.append( [this]( std::string_view layout ) {
		for ( size_t i = 0; i < layoutsList_.size(); ++i ) {
			if ( layoutsList_[i].shortName != layout ) continue;
			layoutId_ = i;
			break;
		}
		onLayoutChanged( layout );
	} );
	backend_->onLayoutListChanged.append( [this]( const std::vector<LayoutNames> &layouts ) {
		layoutsList_ = layouts;
		onLayoutListChanged( layouts );
	} );

	layoutsList_ = backend_->getLayoutsList();
	const auto activeLayout = backend_->getActiveLayout();
}
