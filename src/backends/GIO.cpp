#include "GIO.h"

#include <regex>

#include "IsoCodes.h"

namespace {
	// 'sources' and 'mru-sources' example: [('xkb', 'us'), ('xkb', 'ru'), ('ibus', 'm17n:ru:kbd')]
	std::regex re_layout( R"(^(?:\w+:)?(\w+)(?::\w+)?$)" );
} // namespace

GIO::GIO() :
	settings_( g_settings_new( "org.gnome.desktop.input-sources" ), g_object_unref ),
	loop_( g_main_loop_new( nullptr, FALSE ), g_main_loop_unref ) {
	isoCodes_ = std::make_unique<IsoCodes>();

	g_signal_connect( settings_.get(), "changed", reinterpret_cast<GCallback>( &GIO::onSettingsChanged ), this );

	thread_ = std::thread( &g_main_loop_run, loop_.get() );

	onSettingsChanged( settings_.get(), "sources", this );
	onSettingsChanged( settings_.get(), "mru-sources", this );
}

GIO::~GIO() {
	if ( g_main_loop_is_running( loop_.get() ) != FALSE ) g_main_loop_quit( loop_.get() );
	if ( thread_.joinable() ) thread_.join();
}

std::vector<LayoutWatcher::LayoutNames> GIO::getLayoutsList() const {
	return getLayoutsList( settings_.get(), isoCodes_.get() );
}

std::string_view GIO::getActiveLayout() const {
	auto *sources = g_settings_get_value( settings_.get(), "mru-sources" );
	const auto list = parseSources( sources );
	g_variant_unref( sources );

	activeLayout_ = list[0];
	return activeLayout_;
}

void GIO::onSettingsChanged( GSettings *settings, const gchar *key, GIO *This ) {
	using namespace std::string_view_literals;
	static constexpr auto kSources = "sources"sv;
	static constexpr auto kMruSources = "mru-sources"sv;

	if ( key == kSources ) return This->onLayoutListChanged( getLayoutsList( settings, This->isoCodes_.get() ) );

	if ( key == kMruSources ) {
		auto *sources = g_settings_get_value( settings, "mru-sources" );
		const auto list = parseSources( sources );
		g_variant_unref( sources );

		return This->onLayoutChanged( list[0] );
	}
}

std::vector<std::string> GIO::parseSources( GVariant *sources ) {
	GVariantIter iter;
	g_variant_iter_init( &iter, sources );

	std::vector<std::string> result;

	const gchar *driver;
	const gchar *layout;
	while ( g_variant_iter_loop( &iter, "(&s&s)", &driver, &layout ) != FALSE ) {
		std::cmatch match;
		if ( std::regex_match( layout, match, re_layout ) ) result.push_back( match.str( 1 ) );
	}

	return result;
}

std::vector<LayoutWatcher::LayoutNames> GIO::getLayoutsList( GSettings *settings, IsoCodes *isoCodes ) {
	auto *sources = g_settings_get_value( settings, "sources" );
	const auto list = parseSources( sources );
	g_variant_unref( sources );

	std::vector<LayoutWatcher::LayoutNames> result;
	for ( auto &&name : list ) {
		LayoutWatcher::LayoutNames layout;
		layout.shortName = name;
		layout.longName = isoCodes->getLanguageName( name );
		layout.displayName = name;
		layout.displayName[0] = static_cast<char>( std::toupper( layout.displayName[0] ) );
		result.push_back( layout );
	}
	return result;
}
