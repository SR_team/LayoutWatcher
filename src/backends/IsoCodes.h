#pragma once

#include <cstddef>
#include <string>
#include <string_view>
#include <unordered_map>

struct IsoCodeKey {
	char alpha_2[3];
	char alpha_3[4];
	IsoCodeKey( std::string_view alpha_2, std::string_view alpha_3 = {} );
	IsoCodeKey( const IsoCodeKey &other );
	IsoCodeKey( IsoCodeKey &&other ) noexcept;

	bool operator==( std::string_view rhs ) const { return alpha_2 == rhs || alpha_3 == rhs; }
	bool operator==( const IsoCodeKey &rhs ) const {
		return std::string_view{ alpha_2 } == rhs.alpha_2 && std::string_view{ alpha_3 } == rhs.alpha_3;
	}

	bool operator<( const IsoCodeKey &rhs ) const {
		return std::string_view{ alpha_2 } < rhs.alpha_2 && std::string_view{ alpha_3 } < rhs.alpha_3;
	}
};
template<> struct std::hash<IsoCodeKey> {
	std::size_t operator()( IsoCodeKey const &key ) const noexcept {
		std::string str_key = key.alpha_2 + std::string( ":" ) + key.alpha_3;
		return std::hash<std::string>{}( str_key );
	}
};

class IsoCodes {
	static constexpr auto kIsoCodesPath = "/usr/share/iso-codes/json/iso_639-2.json";

public:
	IsoCodes();

	std::string getLanguageName( std::string_view iso_code ) const;
	std::string getLanguageCode( const std::string &language ) const;

protected:
	std::unordered_map<IsoCodeKey, std::string> languages_;
	std::unordered_map<std::string, IsoCodeKey> shortes_;
};
