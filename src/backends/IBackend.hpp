#pragma once

#include "LayoutWatcher.h"

class IBackend {
public:
	virtual ~IBackend() {};

	virtual std::vector<LayoutWatcher::LayoutNames> getLayoutsList() const = 0;
	virtual std::string_view getActiveLayout() const = 0;

	// Signals
	eventpp::CallbackList<void( std::string_view )> onLayoutChanged;
	eventpp::CallbackList<void( const std::vector<LayoutWatcher::LayoutNames> & )> onLayoutListChanged;
};