#include "X11.h"

#include <iostream>

#include <X11/XKBlib.h>

X11::X11( std::chrono::milliseconds updateTime ) : updateTime_( updateTime ) {
	thread_ = std::jthread( [this]( std::stop_token stoken ) { watcher( stoken ); } );
}

X11::~X11() {
	thread_.request_stop();
	thread_.join();
	freeKeyboard();
}

std::vector<LayoutWatcher::LayoutNames> X11::getLayoutsList() const {
	return languagesToLayouts( getLanguages() );
}

std::string_view X11::getActiveLayout() const {
	for ( size_t i = 0; i < languages_.size(); ++i ) {
		if ( languages_[i].group != activeGroup_ ) continue;

		activeLayout_ = languages_[i].name.substr( 0, 2 );
		std::transform( activeLayout_.begin(), activeLayout_.end(), activeLayout_.begin(), ::tolower );

		return activeLayout_;
	}
	return "";
}

void X11::updateDisplayAddr() {
	const auto *display_addr = getenv( "DISPLAY" );
	if ( display_addr == nullptr || *display_addr == '\0' ) return;
	if ( displayAddr_ != display_addr ) {
		displayAddr_ = display_addr;
		openKeyboard( displayAddr_ );
		updateLayouts();
	}
}

std::vector<X11::Language> X11::getLanguages() const {
	std::vector<Language> languages;
	if ( keyboard_ == nullptr ) return languages;

	Atom current_group;
	for ( unsigned long group : static_cast<XkbDescPtr>( keyboard_ )->names->groups ) {
		current_group = group;
		if ( current_group == 0u ) continue;

		auto *group_name = XGetAtomName( static_cast<Display *>( display_ ), current_group );
		if ( group_name == nullptr || *group_name == '\0' ) continue;

		Language lang;
		lang.name = group_name;
		lang.group = current_group;

		languages.push_back( lang );
		XFree( group_name );
	}

	return languages;
}

std::vector<LayoutWatcher::LayoutNames> X11::languagesToLayouts( const std::vector<Language> &languages ) {
	std::vector<LayoutWatcher::LayoutNames> layoutsList;
	for ( auto &&lang : languages ) {
		LayoutWatcher::LayoutNames layout;
		layout.longName = lang.name;
		layout.displayName = layout.longName.substr( 0, 2 );
		layout.shortName = layout.displayName;
		std::transform( layout.shortName.begin(), layout.shortName.end(), layout.shortName.begin(), ::tolower );
		layoutsList.push_back( layout );
	}
	return layoutsList;
}

void X11::updateLayouts() {
	if ( keyboard_ == nullptr ) return;

	auto languages = getLanguages();
	if ( !languages.empty() && languages_ != languages ) {
		languages_ = languages;
		auto layoutsList = languagesToLayouts( languages );
		onLayoutListChanged( layoutsList );
	}
}

bool X11::updateLayoutId( unsigned long group ) {
	if ( group == 0u ) return false;
	if ( group == activeGroup_ ) return true;
	activeGroup_ = group;
	for ( size_t i = 0; i < languages_.size(); ++i ) {
		if ( languages_[i].group != group ) continue;

		auto shortName = languages_[i].name.substr( 0, 2 );
		std::transform( shortName.begin(), shortName.end(), shortName.begin(), ::tolower );

		onLayoutChanged( shortName );
		return true;
	}
	return false;
}

unsigned long X11::getActiveGroup() {
	if ( keyboard_ == nullptr ) return 0;
	XkbStateRec state;
	if ( XkbGetState( static_cast<Display *>( display_ ), XkbUseCoreKbd, &state ) != Success ) {
		std::cerr << "Can't get active layout group" << std::endl;
		return 0;
	}
	return static_cast<XkbDescPtr>( keyboard_ )->names->groups[state.group];
}

bool X11::freeKeyboard() {
	if ( keyboard_ == nullptr ) return false;

	XkbFreeKeyboard( static_cast<XkbDescPtr>( keyboard_ ), 0, True );
	keyboard_ = nullptr;
	return true;
}

void X11::watcher( std::stop_token &stoken ) {
	while ( !stoken.stop_requested() ) {
		updateDisplayAddr();
		auto group = getActiveGroup();
		if ( !updateLayoutId( group ) ) {
			openKeyboard( displayAddr_ );
			updateLayouts();
		}
		std::this_thread::sleep_for( updateTime_ );
	}
}

void X11::openKeyboard( std::string &display ) {
	int result;
	display_ = XkbOpenDisplay( display.data(), NULL, NULL, NULL, NULL, &result );
	if ( display_ == nullptr ) {
		std::cerr << "Can't open display" << std::endl;
		return;
	}
	freeKeyboard();
	keyboard_ = XkbAllocKeyboard();
	if ( keyboard_ == nullptr ) {
		std::cerr << "Can't allocate keyboard" << std::endl;
		return;
	}
	if ( XkbGetNames( static_cast<Display *>( display_ ), XkbGroupNamesMask, static_cast<XkbDescPtr>( keyboard_ ) ) != Success ) {
		std::cerr << "Can't get keyboard names" << std::endl;
		freeKeyboard();
	}
}
