#pragma once

#include <memory>
#include <string>
#include <thread>
#include <vector>

#include <gio/gio.h>

#include "IBackend.hpp"

class IsoCodes;

class GIO : public IBackend {
public:
	GIO();
	~GIO() override;

	std::vector<LayoutWatcher::LayoutNames> getLayoutsList() const override;
	std::string_view getActiveLayout() const override;

protected:
	std::unique_ptr<IsoCodes> isoCodes_;
	std::unique_ptr<GSettings, decltype( &g_object_unref )> settings_;
	std::unique_ptr<GMainLoop, decltype( &g_main_loop_unref )> loop_;
	std::thread thread_;
	mutable std::string activeLayout_;

	static void onSettingsChanged( GSettings *settings, const gchar *key, GIO *This );
	static std::vector<std::string> parseSources( GVariant *sources );
	static std::vector<LayoutWatcher::LayoutNames> getLayoutsList( GSettings *settings, IsoCodes *isoCodes );
};
