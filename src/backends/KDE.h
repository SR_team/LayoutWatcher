#pragma once

#include "IBackend.hpp"

namespace sdbus {
	class IProxy;
	class IConnection;
} // namespace sdbus

class KDE : public IBackend {
public:
	KDE();
	~KDE() override;

	std::vector<LayoutWatcher::LayoutNames> getLayoutsList() const override;
	std::string_view getActiveLayout() const override;

protected:
	std::unique_ptr<sdbus::IConnection> connection_;
	std::unique_ptr<sdbus::IProxy> proxy_;
	std::vector<LayoutWatcher::LayoutNames> layoutsList_;
	unsigned int layoutId_;

	void initialize( const char *service );
	void updateLayouts();
	void layoutChanged( unsigned int id );
	void layoutListChanged();
};