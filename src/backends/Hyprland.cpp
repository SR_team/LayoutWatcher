#include "Hyprland.h"

#include <cstring>
#include <filesystem>
#include <string_view>

#include <fcntl.h>
#include <sys/socket.h>
#include <sys/un.h>

#include "IsoCodes.h"

using namespace std::string_view_literals;

static constexpr auto kBufferSize = 4096;
static constexpr auto kLayoutEvent = "activelayout>>"sv;

Hyprland::Hyprland( std::chrono::milliseconds updateTime ) : updateTime_( updateTime ) {
	const auto *runtime_dir = getenv( "XDG_RUNTIME_DIR" );
	if ( runtime_dir == nullptr ) throw std::runtime_error( "XDG_RUNTIME_DIR environment variable is not set" );

	const auto *instance = getenv( "HYPRLAND_INSTANCE_SIGNATURE" );
	if ( instance == nullptr ) throw std::runtime_error( "HYPRLAND_INSTANCE_SIGNATURE environment variable is not set" );

	std::string socket_path = std::string( runtime_dir ) + "/hypr/" + instance + "/.socket2.sock";
	if ( !std::filesystem::exists( socket_path ) ) throw std::runtime_error( "Hyprland socket file not found" );

	thread_ = std::jthread( [this, socket_path]( std::stop_token stoken ) { worker( stoken, socket_path ); } );
}

Hyprland::~Hyprland() {
	if ( thread_.joinable() ) {
		thread_.request_stop();
		thread_.join();
	}
}

std::vector<LayoutWatcher::LayoutNames> Hyprland::getLayoutsList() const {
	std::scoped_lock lock( mutex_ );
	return layoutsList_;
}

std::string_view Hyprland::getActiveLayout() const {
	std::scoped_lock lock( mutex_ );
	return activeLayout_;
}

void Hyprland::worker( std::stop_token stoken, const std::string &socket_path ) {
	auto sock = socket( AF_UNIX, SOCK_STREAM, 0 );
	if ( sock < 0 ) return;

	const auto flags = fcntl( sock, F_GETFL, 0 );
	fcntl( sock, F_SETFL, flags | O_NONBLOCK );

	struct sockaddr_un serv_addr;
	memset( &serv_addr, 0, sizeof( serv_addr ) );
	serv_addr.sun_family = AF_UNIX;
	strncpy( serv_addr.sun_path, socket_path.c_str(), sizeof( serv_addr.sun_path ) - 1 );

	if ( connect( sock, reinterpret_cast<struct sockaddr *>( &serv_addr ), sizeof( serv_addr ) ) < 0 ) {
		if ( errno != EINPROGRESS ) return;
	}

	char buffer[kBufferSize] = { 0 };
	while ( !stoken.stop_requested() ) {
		const auto len = read( sock, buffer, kBufferSize - 1 );

		if ( len <= 0 ) {
			if ( len < 0 && ( errno == EAGAIN || errno == EWOULDBLOCK ) ) {
				std::this_thread::sleep_for( updateTime_ );
				continue;
			}
			break;
		}

		buffer[len] = '\0';
		auto str = std::string_view{ buffer, static_cast<size_t>( len ) };
		std::size_t pos = 0;
		do {
			pos = str.find( '\n', pos );
			const auto message = str.substr( 0, pos );
			str = str.substr( pos + 1 );
			if ( !message.starts_with( kLayoutEvent ) ) continue;

			const auto data = message.substr( kLayoutEvent.length() );
			const auto split_pos = data.find( ',' );
			if ( split_pos == std::string_view::npos ) continue;

			std::string_view keyboard = data.substr( 0, split_pos ); // TODO: Notify keyboard too
			std::string_view layout = data.substr( split_pos + 1 );
			std::scoped_lock lock( mutex_ );

			if ( !isoCodes_ ) isoCodes_ = std::make_unique<IsoCodes>();
			auto code = isoCodes_->getLanguageCode( std::string{ layout } );
			if ( code.empty() ) {
				code = layout.substr( 0, 3 );
				std::transform( code.begin(), code.end(), code.begin(), ::tolower );
			}

			const auto layouts_count = layoutsList_.size();
			auto it = std::find_if( layoutsList_.begin(), layoutsList_.end(), [layout]( const LayoutWatcher::LayoutNames &layout_name ) {
				return layout_name.longName == layout;
			} );
			if ( it == layoutsList_.end() ) {
				LayoutWatcher::LayoutNames layout_name;
				layout_name.shortName = code;
				layout_name.longName = layout;
				layout_name.displayName = code;
				layout_name.displayName[0] = static_cast<char>( std::toupper( code[0] ) );

				layoutsList_.push_back( layout_name );
				it = std::prev( layoutsList_.end() );
			}

			activeLayout_ = it->shortName;
			onLayoutChanged( it->shortName );
			if ( layouts_count != layoutsList_.size() ) onLayoutListChanged( layoutsList_ );
		} while ( pos != std::string_view::npos );
	}
}
