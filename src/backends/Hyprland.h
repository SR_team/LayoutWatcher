#pragma once

#include <chrono>
#include <memory>
#include <mutex>
#include <string>
#include <thread>

#include "IBackend.hpp"

class IsoCodes;

class Hyprland : public IBackend {
public:
	Hyprland( std::chrono::milliseconds updateTime );
	~Hyprland() override;

	std::vector<LayoutWatcher::LayoutNames> getLayoutsList() const override;
	std::string_view getActiveLayout() const override;

protected:
	const std::chrono::milliseconds updateTime_;
	std::jthread thread_;
	mutable std::mutex mutex_;
	std::unique_ptr<IsoCodes> isoCodes_;
	std::vector<LayoutWatcher::LayoutNames> layoutsList_;
	mutable std::string activeLayout_;

	void worker( std::stop_token stoken, const std::string &socket_path );
};