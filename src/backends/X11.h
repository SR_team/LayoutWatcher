#pragma once

#include <chrono>
#include <thread>

#include "IBackend.hpp"

class X11 : public IBackend {
public:
	X11( std::chrono::milliseconds updateTime );
	~X11() override;

	std::vector<LayoutWatcher::LayoutNames> getLayoutsList() const override;
	std::string_view getActiveLayout() const override;

protected:
	struct Language {
		ulong group;
		std::string name;

		bool operator==( const Language &rhs ) const { return group == rhs.group; }
	};

	std::chrono::milliseconds updateTime_;
	std::string displayAddr_;
	unsigned long activeGroup_ = 0;
	std::vector<Language> languages_;
	void *display_;
	void *keyboard_ = nullptr;
	std::jthread thread_;
	mutable std::string activeLayout_;

	void updateDisplayAddr();
	std::vector<Language> getLanguages() const;
	static std::vector<LayoutWatcher::LayoutNames> languagesToLayouts( const std::vector<Language> &languages );
	void updateLayouts();
	bool updateLayoutId( unsigned long group );

	unsigned long getActiveGroup();
	bool freeKeyboard();

	virtual void watcher( std::stop_token &stoken );

	void openKeyboard( std::string &display );
};
