#include "IsoCodes.h"

#include <cstring>
#include <fstream>
#include <iostream>
#include <stdexcept>

#include <nlohmann/json.hpp>

IsoCodeKey::IsoCodeKey( std::string_view alpha_2, std::string_view alpha_3 ) {
	if ( alpha_2.length() > 2 || alpha_3.length() > 3 ) {
		const auto code = std::string( alpha_2.length() > 2 ? alpha_2 : alpha_3 );
		throw std::runtime_error( "Invalid ISO 639-2 code \"" + code + "\"" );
	}

	std::strncpy( this->alpha_2, alpha_2.data(), alpha_2.length() );
	std::strncpy( this->alpha_3, alpha_3.data(), alpha_3.length() );

	this->alpha_2[alpha_2.length()] = '\0';
	this->alpha_3[alpha_3.length()] = '\0';
}

IsoCodeKey::IsoCodeKey( const IsoCodeKey &other ) {
	std::strncpy( this->alpha_2, other.alpha_2, 2 );
	std::strncpy( this->alpha_3, other.alpha_3, 3 );

	this->alpha_2[2] = '\0';
	this->alpha_3[3] = '\0';
}

IsoCodeKey::IsoCodeKey( IsoCodeKey &&other ) noexcept {
	std::strncpy( this->alpha_2, other.alpha_2, 2 );
	std::strncpy( this->alpha_3, other.alpha_3, 3 );

	this->alpha_2[2] = '\0';
	this->alpha_3[3] = '\0';

	other.alpha_2[0] = '\0';
	other.alpha_3[0] = '\0';
}

IsoCodes::IsoCodes() {
	std::ifstream stream( kIsoCodesPath );
	if ( !stream.is_open() ) throw std::runtime_error( "Failed to open iso-codes json file" );
	nlohmann::json json;
	stream >> json;

	const auto languages = json["639-2"];
	for ( const auto &lang : languages ) {
		const auto alpha_2 = lang.value( "alpha_2", "" );
		const auto alpha_3 = lang.value( "alpha_3", "" );
		if ( alpha_2.length() > 2 || alpha_3.length() > 3 ) continue;

		languages_[IsoCodeKey{ alpha_2, alpha_3 }] = lang.value( "name", "" );
		shortes_.insert( { lang.value( "name", "" ), IsoCodeKey{ alpha_2, alpha_3 } } );

		if ( !alpha_2.empty() ) std::cout << "ISO 639-2: " << alpha_2 << " -> " << lang.value( "name", "" ) << std::endl;
	}
}

std::string IsoCodes::getLanguageName( std::string_view iso_code ) const {
	if ( iso_code.length() > 3 ) return "";

	auto it = std::find_if( languages_.begin(), languages_.end(), [iso_code]( const auto &lang ) {
		return iso_code == lang.first.alpha_2 || iso_code == lang.first.alpha_3;
	} );

	return it != languages_.end() ? it->second : "";
}

std::string IsoCodes::getLanguageCode( const std::string &language ) const {
	auto it = shortes_.find( language );
	if ( it == shortes_.end() ) return "";
	return *it->second.alpha_2 != '\0' ? it->second.alpha_2 : it->second.alpha_3;
}
