#include "KDE.h"

#include <sdbus-c++/sdbus-c++.h>

namespace consts {
	constexpr auto kDBusServiceKbd = "org.kde.keyboard";
	constexpr auto kDBusServiceKWin = "org.kde.KWin";
	constexpr auto kDBusPath = "/Layouts";
	constexpr auto kDBusInterface = "org.kde.KeyboardLayouts";
	// DBus signals
	constexpr auto kDBusSigLayout = "layoutChanged";
	constexpr auto kDBusSigLayoutList = "layoutListChanged";
	// DBus methods
	constexpr auto kDBusMethodLayout = "getLayout";
	constexpr auto kDBusMethodLayoutList = "getLayoutsList";
} // namespace consts

KDE::KDE() {
	try {
		initialize( consts::kDBusServiceKbd );
	} catch ( const sdbus::Error &e ) {
		initialize( consts::kDBusServiceKWin );
	}
	connection_->enterEventLoopAsync();
}

KDE::~KDE() {
	connection_->leaveEventLoop();
}

std::vector<LayoutWatcher::LayoutNames> KDE::getLayoutsList() const {
	return layoutsList_;
}

std::string_view KDE::getActiveLayout() const {
	return layoutsList_[layoutId_].shortName;
}

void KDE::initialize( const char *service ) {
	connection_ = sdbus::createSessionBusConnection();
	proxy_ = sdbus::createProxy( *connection_, sdbus::ServiceName{ service }, sdbus::ObjectPath{ consts::kDBusPath } );
	proxy_->uponSignal( consts::kDBusSigLayout ).onInterface( consts::kDBusInterface ).call( [this]( unsigned int layoutId ) {
		layoutChanged( layoutId );
	} );
	proxy_->uponSignal( consts::kDBusSigLayoutList ).onInterface( consts::kDBusInterface ).call( [this] { layoutListChanged(); } );

	updateLayouts();

	proxy_->callMethod( consts::kDBusMethodLayout ).onInterface( consts::kDBusInterface ).storeResultsTo( layoutId_ );
}

void KDE::updateLayouts() {
	if ( !proxy_ ) return;
	std::vector<sdbus::Struct<std::string, std::string, std::string>> layouts;
	proxy_->callMethod( consts::kDBusMethodLayoutList ).onInterface( consts::kDBusInterface ).storeResultsTo( layouts );
	for ( auto &&layout : layouts ) {
		auto short_name = layout.get<0>();
		auto display_name = layout.get<1>();
		auto long_name = layout.get<2>();

		if ( display_name.empty() ) {
			display_name = long_name;
			auto space_pos = display_name.find( ' ' );
			if ( space_pos != std::string::npos ) {
				display_name = display_name.substr( 0, space_pos );
			}
		}
		layoutsList_.emplace_back( std::move( short_name ), std::move( display_name ), std::move( long_name ) );
	}
}

void KDE::layoutChanged( unsigned int id ) {
	layoutId_ = id;
	onLayoutChanged( layoutsList_[id].shortName );
}

void KDE::layoutListChanged() {
	updateLayouts();
	onLayoutListChanged( layoutsList_ );
	proxy_->callMethod( consts::kDBusMethodLayout ).onInterface( consts::kDBusInterface ).storeResultsTo( layoutId_ );
	layoutChanged( layoutId_ );
}
